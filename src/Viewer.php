<?php
/**
 * Grid Phalcon PHP extension
 * PHP version 5.4+
 *
 * @license MIT http://opensource.org/licenses/MIT
 * @author  someson <somevov4ik@gmail.com>
 */

namespace Someson\Grid;

use Phalcon\DI;
use Phalcon\Mvc\View;
use Phalcon\Mvc\User\Component;
use Someson\Grid\Viewer\Fields\Simple;
use Someson\Grid\Viewer\Fields\Custom;
use Someson\Grid\Viewer\Paginator;
use Someson\Grid\Viewer\Searcher;
use Someson\Grid\Viewer\Header;
use Someson\Grid\Viewer\Filter;

/**
 * Class Viewer
 * Used to output listing data.
 * @package Grid
 *
 * @method Viewer\Paginator  getPaginator()
 * @method bool              isPaginator()
 * @method Viewer\Searcher   getSearcher()
 * @method bool              isSearcher()
 * @method Viewer\Searcher   getFilter()
 * @method TraitConfigurable setOptions(array $options)
 */
class Viewer extends Component
{
	use TraitConfigurable, TraitStructure;

	private $_persistHash, $_config;
	protected $_body, $_status;
	protected $_orderField, $_orderType;
	/** @var Header[] */
	protected $_head;
	/** @var IDataProvider|TraitConfigurable */
	protected $_dataProvider;
	/** @var array */
	protected $_structure;
	/** @var Paginator */
	protected $_paginator;
	/** @var Searcher */
	protected $_searcher;
	/** @var Filter */
	protected $_filter;

	protected static $_fieldsDelimiter = '+';

	public function __construct($config)
	{
		$this->_config = $config;
		$this->_body = [];
		$this->_head = [];
		$this->_persistHash = $this->router->getRewriteUri();
		$this->initTools();

		$request = $this->request->getPost();
		$this->setOptions([
			'request'     => Header::isValidPost($request) ? $request : [],
			'paginator'   => true,
			'listLimiter' => true,
			'searcher'    => true,
			'filter'      => true,
			'status'      => true,
			'sortable'    => true,
		]);
	}

	/**
	 * Initialize Paginator, Listlimiter, Searcher, Filter
	 */
	protected function initTools()
	{
		$paginator = $limiter = $searcher = $filter = [];
		if (isset($this->_config->tools->elements)) {
			$own = $this->_config->tools->elements;
			if (isset($own->paginator))   $paginator = $own->paginator->toArray();
			if (isset($own->listLimiter)) $limiter   = $own->listLimiter->toArray();
			if (isset($own->searcher))    $searcher  = $own->searcher->toArray();
			if (isset($own->filter))      $filter    = $own->filter->toArray();
		}
		$this->_filter    = new Filter();
		$this->_paginator = new Paginator($paginator, $limiter);
		$this->_searcher  = new Searcher();
		$this->_searcher->setOptions($searcher);
	}

	/**
	* Defines tools position: top, bottom or both.
	* Used to output the html block in defined positions toward the grid listing.
	*
	* @param string $position Position to place the rendered tools block top|bottom
	* @return boolean
	*/
	public function toolsPosition($position)
	{
		if (in_array($position, ['top', 'bottom'])) {
			if (isset($this->_config->tools->position)) {
				$pos = $this->_config->tools->position;
				if (isset($pos->$position)) return !!$pos->$position;
			} else return true;
		}
		return false;
	}

	public function toolsActive()
	{
		return $this->getOptions('paginator')
			|| $this->getOptions('listLimiter')
			|| $this->getOptions('searcher');
	}

	/**
	* Defines the grid structure.
	* Structure values are field objects Scalar|Field|Custom
	*
	* @param IDataProvider $dataProvider Data provider
	* @param array         $structure    Supposed structure [label => Object|scalar]
	*
	* @return self
	*/
	public function create(IDataProvider $dataProvider, array $structure)
	{
		$this->_dataProvider = $dataProvider;
		$this->_structure = $structure;
		$columns  = self::_getUsedColumns($this->_structure);
		$compiled = self::_getReducedColumns($columns['complex'], self::$_fieldsDelimiter);
		$this->setOptions(['columns' => $columns['simple']]);

		foreach ($this->_structure as $label => $cell) {
			$current = isset($compiled[$label]) ? $compiled[$label] : false;
			$this->_head[$label] = new Header($label, $current);
			if (is_array($cell) && isset($cell['options'])) {
				$this->_head[$label]->setOptions($cell['options']);
			}
			if (is_scalar($cell) || ($cell instanceof Custom)) {
				$this->_head[$label]->setOption('sortable', false);
			}
			if (($cell instanceof Simple) && $cell->getType() == Viewer\Fields\Scalar::TYPE_HIDDEN) {
				$this->_head[$label]->setOption('hidden', true);
			}
		}
		if ($this->isSearcher() && $compiled) {
			$this->_searcher->setItems(array_flip($compiled));
		}
		$request = $this->getOptions('request');
		if ($request) {  // if header clicked
			// postalCode.town --> postalCode
			$field = explode(self::$_fieldsDelimiter, $request['field'])[0];
			$orderType = $request['order'];
			foreach ($this->_head as $head) {
				if ($head->getField() !== $request['field']) continue;
				$head->setEnabled(true);
				$head->setOption('order', $orderType);
				break;
			}
			$this->_setOrder($field, $orderType);
			// save session for using in pager/searcher/limiter
			$this->persistent->orderedBy = [
				// admin/post/view/new => postalCode.town ASC
				$this->_persistHash => $request['field'].' '.$request['order']
			];
		} else { // use session
			if (isset($this->persistent->orderedBy[$this->_persistHash])) {
				list($fields, $orderType) = explode(' ', $this->persistent->orderedBy[$this->_persistHash]);
				// postalCode.town --> postalCode
				$field = explode(self::$_fieldsDelimiter, $fields)[0];
				foreach ($this->_head as $head) {
					if ($head->getField() !== $fields) continue;
					$head->setEnabled(true);
					$head->setOption('order', $orderType);
					break;
				}
				$this->_setOrder($field, $orderType);
			}
		}
		return $this;
	}

	public function getLabel($name) {
		if (!in_array($name, array_keys($this->_head))) {
			throw new Exception(sprintf('Label %s is not defined', $name));
		}
		return $this->_head[$name];
	}

	/**
	 * EXPERIMENTAL
	 * ------------
	 * @param mixed $model
	 * @param array $structure
	 * @return $this
	 * @throws \Someson\Grid\Exception
	 */
	public function createFromModel($model, array $structure)
	{
		$this->_dataProvider = DataProvider::create(IDataProvider::TYPE_MODEL, $model);
		return $this->create($this->_dataProvider, $structure);
	}

	/**
	 * EXPERIMENTAL
	 * ------------
	 * @param string $castName
	 * @param string|callable $columnName
	 * @return Simple
	 */
	public function simple($castName, $columnName)
	{
		$type = Simple::getCast($castName);
		return new Simple($type, $columnName);
	}

	/**
	 * EXPERIMENTAL
	 * ------------
	 * @param string $castName
	 * @param string|callable $columnName
	 * @return Custom|Simple
	 */
	public function field($castName, $columnName)
	{
		$type = Simple::getCast($castName);
		if (is_callable($columnName)) {
			return new Custom($type, $columnName);
		}
		return new Simple($type, $columnName);
	}

	private function _setOrder($field, $orderType)
	{
		$order = $this->_dataProvider->orderCriteria($field, $orderType);
		$this->_dataProvider->setOption('order', $order); // postalCode.town ASC
	}

	/**
	* Orders the data by label. Label is one of keys used in structure.
	* If composite fields used the first field taken.
	*
	* @param string      $label      Header name of output column
	* @param string|null $orderType  Type of order: ASC or DESC case insensitive
	*
	* @return $this
	* @throws \Someson\Grid\Exception
	*/
	public function orderByLabel($label, $orderType = null)
	{
		if (!$this->_dataProvider) throw new Exception('Structure is not defined');

		$sortableHeads = array_filter($this->_head, function(Header $head) { return !!$head->getValue(); });
		$labels = array_map(function(Header $head) { return $head->getValue(); }, $sortableHeads);

		if (!in_array($label, $labels))
			throw new Exception(sprintf("[%s] is not in the list of column labels", $label));
		if (!$this->getOptions('request')
			&& !isset($this->persistent->orderedBy[$this->_persistHash])) {

			$orderType = $orderType ?: 'asc';
			$field = $label;
			foreach ($this->_head as $head) {
				if ($label !== $head->getLabel()) continue;
				$head->setEnabled(true);
				$field = $head->getField();
				break;
			}
			if (!$field) throw new Exception(sprintf('Field name cannot be defined for label [%s]', $label));
			$this->_setOrder($field, $orderType);
		}
		return $this;
	}

	/**
	 * Orders the data by DB field name.
	 *
	 * @param string      $fieldName
	 * @param string|null $orderType  Type of order: ASC or DESC case insensitive
	 * @return $this
	 */
	public function orderByField($fieldName, $orderType = null)
	{
		if (!$this->getOptions('request') && !isset($this->persistent->orderedBy[$this->_persistHash])) {
			$this->_setOrder($fieldName, $orderType);
		}
		return $this;
	}

	/**
	 * Filters the data.
	 * Structure values are field objects string|Field|Custom
	 *
	 * @param string $criteria MySql-like condition
	 * @param array $bind
	 *
	 * @throws \Someson\Grid\Exception
	 * @return $this
	 */
	public function where($criteria, $bind = null)
	{
		if (!$this->_dataProvider) throw new Exception('Structure is not defined');
		$this->_dataProvider->setOption('conditions', $criteria);
		if (is_array($bind) && $bind) $this->_dataProvider->setOption('bind', $bind);
		return $this;
	}

	public function count()
	{
		return count($this->_body);
	}

	/**
	 * Gets the final result listing
	 * @return $this
	 * @throws \Someson\Grid\Exception
	 */
	public function resolve()
	{
		if (!$this->_dataProvider) throw new Exception('Structure is not defined');
		if ($this->isPaginator()) {
			$this->_paginator->init($this->_dataProvider);
			if ($this->_paginator->getTotalItems() <= $this->_paginator->getListLimiter()->getMinLimit()) {
				$this->setOptions([
					'paginator'   => false,
					'listLimiter' => false,
					'searcher'    => false,
				]);
			}
			if (!$this->getOptions('listLimiter'))
				$this->_paginator->getListLimiter()->setEnabled(false);

			$rows = $this->_paginator->getItems();
			$this->_status = [
				'current' => $this->_paginator->getCurrent(),
				'pages'   => $this->_paginator->getTotalPages(),
				'items'   => $this->_paginator->getTotalItems(),
			];
		} else {
			$this->setOption('listLimiter', false);
			$this->_dataProvider->setOption('limit', 100);
			$rows = $this->_dataProvider->getData();
			$count = count($rows);
			$this->_status = [
				'current' => 1,
				'pages'   => ceil($count/100),
				'items'   => $count,
			];
		}
		$i = 0;
		$itemInfo = $this->_status + ['limit' => (int) $this->_paginator->getListLimiter()->getCurrent()];
		$this->_status = (object) $this->_status;
		foreach ($rows as $row) {
			$this->_body[$i] = [];
			$linkable = [];
			foreach ($this->_structure as $key => $cell) {
				$this->_body[$i][$key] = self::_customize($cell, $row, $itemInfo + ['index' => $i]);
				if ($cell instanceof ILinkable) $linkable[] = $this->_body[$i][$key];
				elseif ($cell instanceof ButtonGroup) {
					foreach ($this->_body[$i][$key]->getCollection() as $button)
						$linkable[] = $button;
				}
			}
			/** @var \Someson\Grid\Button $obj */
			foreach ($linkable as $obj) {
				$values = [];
				$keys = $obj->getBoundKeys();
				foreach ($keys as $key) $values[$key] = $this->_body[$i][$key]->getValue();
				$obj->bind($values);
			}
			++$i;
		}
		if (!$this->getOptions('sortable')) {
			foreach ($this->_head as $head) $head->setOption('sortable', false);
		}
		return $this;
	}

	public function removeByLabel($label)
	{
		unset($this->_structure[$label], $this->_head[$label]);
	}

	public function createButton($name, $uri, $htmlOptions = [])
	{
		$button = new Button($name, $uri);
		$button->htmlOptions($htmlOptions);
		return $button;
	}

	public function createButtonGroup(array $buttons)
	{
		return new ButtonGroup($buttons);
	}

	public function createViewButton($uri, $htmlOptions = [])
	{
		return $this->createButton('<span class="glyphicon glyphicon-folder-open"></span>', $uri, $htmlOptions);
	}

	public function createEditButton($uri, $htmlOptions = [])
	{
		return $this->createButton('<span class="glyphicon glyphicon-edit"></span>', $uri, $htmlOptions);
	}

	public function render($where, $what, $params = [])
	{
		$view = DI::getDefault()->getShared('view');
		$view->start();
		$view->setRenderLevel(View::LEVEL_LAYOUT);
		$view->setVars($params);
		$view->render($where, $what);
		$view->setRenderLevel(View::LEVEL_MAIN_LAYOUT);
		$view->finish();
		$content = $view->getContent();
		$view = null;
		return $content;
	}
}
