<?php
/**
 * Grid Phalcon PHP extension
 * PHP version 5.4+
 *
 * @license MIT http://opensource.org/licenses/MIT
 * @author  someson <somevov4ik@gmail.com>
 */

namespace Someson\Grid\Viewer;

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Submit;

use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\StringLength;

use Someson\Grid\Element;

/**
 * Class Searcher
 * Implements search functionality. Used to build html.
 *
 * @package Grid
 */
class Searcher extends Element
{
	protected $_searchFor, $_rangeItems;
	protected static $_counter = 0;

	public function __construct()
	{
		$this->_rangeItems = ['*' => 'All columns'];
		$this->setOptions([
			// default options here
		]);
	}

	public function addItem($value, $label)
	{
		$this->_rangeItems[$value] = $label;
	}

	public function setItems($items)
	{
		foreach ($items as $value => $label) $this->addItem($value, $label);
	}

	public function getItems()
	{
		return $this->_rangeItems;
	}

	/**
	* Implements interface method.
	* Gets the rendered html data for output.
	*
	* @return string
	*/
	public function getValue()
	{
		return $this->render();
	}

	public function render()
	{
		$id = self::$_counter++;

		$form = new Form();
		$form->add((new Text('search-for', ['id' => 'search-for-'.$id]))->addValidators([
			new PresenceOf([
				'message' => 'Empty query',
			]),
			new StringLength([
				'min' => 5,
				'messageMinimum' => 'Query too short (min 5 symbols)',
			]),
		]));
		$form->add(new Select('search-in', $this->getItems(), ['id' => 'search-in-'.$id]));
		$form->add(new Submit('search'));

		$html = $form->render('search-for', [
			'class' => 'form-control search-for',
			'placeholder' => 'Search in',
		]);
		$html.= $form->render('search-in', [
			'class' => 'form-control',
		]);
		$html.= $form->render('search', [
			'value' => 'search',
			'class' => 'btn btn-default search-btn',
		]);
		return $html;
	}

	/**
	* Implements interface method.
	* Gets type of this object, namely the class name.
	*
	* @return string
	*/
	public function getType()
	{
		return __CLASS__;
	}
}
