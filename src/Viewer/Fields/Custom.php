<?php
/**
 * Grid Phalcon PHP extension
 * PHP version 5.4+
 *
 * @license MIT http://opensource.org/licenses/MIT
 * @author  someson <somevov4ik@gmail.com>
 */

namespace Someson\Grid\Viewer\Fields;

/**
 * Class Custom
 * Viewer field, based on callback function to customize the results of
 * current dataset resolved from used DataProvider.
 *
 * @package Grid
 */
class Custom extends Scalar
{
	protected $_callback;

	/**
	* Creates the customized result if $data is an array
	* or customizes result if $data is a callback.
	*
	* @param string          $type Resulting type
	* @param array|callable  $data Array or Callback function
	*/
	public function __construct($type, $data)
	{
		$this->_type = $type;
		if (is_callable($data)) $this->_callback = $data;
		//if (is_array($data)) $this->_value = $data;
	}

	/**
	 * Customize the result output as composition of needed fields
	 * @param array $data       Current resolved dataset.
	 * @param array $itemsInfo  Pagination info
	 * @return Scalar|Table
	 */
	public function customize($data, $itemsInfo = [])
	{
		$customized = $this->_callback ?
			call_user_func_array($this->_callback, [$data, $itemsInfo]) : $data;

		if (is_array($customized) && count($customized))
			return new Table($customized);

		$scalar = new Scalar($customized, $this->getType());
		$this->_value = $scalar->getValue();
		return $scalar;
	}
}
