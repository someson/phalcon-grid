<?php
/**
 * Grid Phalcon PHP extension
 * PHP version 5.4+
 *
 * @license MIT http://opensource.org/licenses/MIT
 * @author  someson <somevov4ik@gmail.com>
 */

namespace Someson\Grid\Viewer\Fields;

use Someson\Grid\TraitPatternBinder;
/**
 * Class Simple
 * Viewer field, based on model data.
 *
 * @package Grid
 */
class Simple extends Scalar
{
	use TraitPatternBinder;

	protected $_field;
	private $_pattern, $_bind;

	public function __construct($type, $field, $bind = [])
	{
		$this->_type = $type;
		if ($this->isValid($bind)) {
			$this->_type = Scalar::TYPE_STR;
			$this->_field = array_values($bind);
			$this->_pattern = $field;
			$this->_bind = $bind;
		} else $this->_field = (array) $field;
	}

	public function getUsedFields()
	{
		return $this->_field;
	}

	public function compile($data)
	{
		$out = count($this->_bind) ? $this->bind($data) : $data[$this->_field[0]];
		$this->_value = $this->convert($out);
	}

	private function bind($value)
	{
		$value = $this->map($value);
		$string = $this->_pattern;
		$matches = self::getMatches($string);
		return count($matches) ? self::getBoundString($value, $matches, $string) : $string;
	}

	// _bind   : array('zip' => 'postalCode', 'city' => 'town');
	// replace : array('postalCode' => 18119, 'town' => 'Rostock')
	// result  : array('zip' => '18119', 'city' => 'Rostock');
	private function map($new)
	{
		$map = [];
		foreach ($this->_bind as $key => $value) $map[$key] = $new[$value];
		return $map;
	}

	private function isValid($structure = [])
	{
		return empty($structure) ? false : !!count($structure);
	}
}
