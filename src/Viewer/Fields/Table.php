<?php
/**
 * Grid Phalcon PHP extension
 * PHP version 5.4+
 *
 * @license MIT http://opensource.org/licenses/MIT
 * @author  someson <somevov4ik@gmail.com>
 */

namespace Someson\Grid\Viewer\Fields;

use Someson\Grid\Exception;
use Phalcon\Tag;

/**
 * Class Table
 * Extends the Custom field class for tabular output.
 *
 * @package Grid
 */
class Table extends Custom
{
	protected $_head;
	/**
	* Extends the Custom class properties, using "array" type for resulting field.
	*
	* @todo Schould throws exception if used arrays are not uniform.
	*
	* @param array|\Closure $data Array or Callback function
	*/
	public function __construct($data)
	{
		parent::__construct(self::TYPE_ARR, $data);

		// possibly 1-dimentional or 2-dimensional array
		//$count = count($data);

		$this->_head = [];
		if (self::isAssocArray($data)) { // 1-dimentional associative
			//-------------------
			//|  a  |  b  |  c  | ... <-- Header
			//-------------------
			//| abc | def | xyz | ...

			$this->_head = array_keys($data);

		} else { // 2-dimensional or 1-dimentional sequential

			if (is_array($data[0])) { // 2-dimensional
			//-------------------
			//|  0  |  1  |  2  | ...
			//-------------------
			//|[...]|[...]|[...]| ... <-- possibly contains Header

				if (self::isAssocArray($data[0]))
					$this->_head = array_keys($data[0]);
			}
			//-------------------
			//|  0  |  1  |  2  | ... <-- NO Header
			//-------------------
			//| abc | def | xyz | ...
		}
/*
		// populate the header variable
		foreach ($this->_value as $value) {
			if (is_array($value)) {
				// only keys of type string used
				foreach ($value as $head => $scalar) {
					if (is_string($head)) $this->_head[] = $head;
				}
				if (count($this->_head) !== count($value)) $this->_head[] = [];
			}
			break;
		}
*/
	}

	public function getHeader()
	{
		return $this->_head;
	}

	public function getValue()
	{
		foreach ($this->_value as &$value) {
			if (is_array($value)) {
				foreach ($value as &$scalar) {
					if (is_array($scalar)) throw new Exception('Max. 2 dimentions of array allowed');
					if (!$scalar instanceof Scalar) $scalar = new Scalar($scalar);
				}
				unset($scalar);
			} else $value = new Scalar($value);
		}
		unset($value);
		return $this->_value;
	}

	public function render()
	{
		$this->htmlOptions(['class' => 'table-inner']);
		$table = Tag::tagHtml('table', $this->getAttr());
		foreach ($this->getValue() as $tr) {
			$table.= Tag::tagHtml('tr');
			foreach ($tr as $td) {
				$table.= Tag::tagHtml('td', $td->getAttr());
				$table.= $td->getType() == self::TYPE_STR ?
					$td->wrapWith('div', ['class' => 'truncated']) : $td->render();
				$table.= Tag::tagHtmlClose('td');
			}
			$table.= Tag::tagHtmlClose('tr');
		}
		$table.= Tag::tagHtmlClose('table');
		return $table;
	}
}
