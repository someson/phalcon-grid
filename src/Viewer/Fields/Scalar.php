<?php
/**
 * Grid Phalcon PHP extension
 * PHP version 5.4+
 *
 * @license MIT http://opensource.org/licenses/MIT
 * @author  someson <somevov4ik@gmail.com>
 */

namespace Someson\Grid\Viewer\Fields;

use Someson\Grid\Element;

/**
 * Class Scalar
 * Viewer field, based on scalar data.
 *
 * @package Grid
 */
class Scalar extends Element
{
	const TYPE_HIDDEN   = 0;
	const TYPE_INT      = 1;
	const TYPE_BOOL     = 2;
	const TYPE_STR      = 3;
	const TYPE_ARR      = 4;
	const TYPE_DATE     = 5;
	const TYPE_TIME     = 6;
	const TYPE_DATETIME = 7;

	protected $_type, $_value;

	public function __construct($value, $type = self::TYPE_HIDDEN)
	{
 		if ($type) $this->_type = $type;
		else {
			if     (is_array($value)) $this->_type = self::TYPE_ARR;
			elseif (is_bool($value))  $this->_type = self::TYPE_BOOL;
			elseif (is_int($value))   $this->_type = self::TYPE_INT;
			elseif (($timestamp = strtotime($value)) === false) $this->_type = self::TYPE_DATETIME;
			else   $this->_type = self::TYPE_STR;
		}
		$this->_value = $this->convert(trim($value));
	}

	/**
	 * @param mixed $value
	 * @return array|bool|int|string
	 */
	public function convert($value)
	{
		if ($value === '') return false;
		switch ($this->_type) {
			case self::TYPE_INT  : $value = intval($value); break;
			case self::TYPE_BOOL : $value = !empty($value); break;
			case self::TYPE_STR  : $value = strval($value); break;
			case self::TYPE_ARR  : $value = (array) $value; break;
			case self::TYPE_DATE : $value = date('d.m.Y', strtotime($value)); break;
			case self::TYPE_TIME : $value = date('H:i:s', strtotime($value)); break;
			case self::TYPE_DATETIME : $value = date('d.m.Y H:i:s', strtotime($value)); break;
			default: break;
		}
		return $value;
	}

	/**
	 * @param string $string
	 * @return mixed
	 */
	public static function getCast($string)
	{
		$map = [
			'hidden'   => self::TYPE_HIDDEN,
			'int'      => self::TYPE_INT,
			'bool'     => self::TYPE_BOOL,
			'string'   => self::TYPE_STR,
			'array'    => self::TYPE_ARR,
			'date'     => self::TYPE_DATE,
			'time'     => self::TYPE_TIME,
			'datetime' => self::TYPE_DATETIME,
		];
		return $map[$string];
	}

	public function getValue()
	{
		return $this->_value;
	}

	public function getType()
	{
		return $this->_type;
	}

	public function isHidden()
	{
		return $this->getType() == self::TYPE_HIDDEN;
	}
}
