<?php
/**
 * Grid Phalcon PHP extension
 * PHP version 5.4+
 *
 * @license MIT http://opensource.org/licenses/MIT
 * @author  someson <somevov4ik@gmail.com>
 */

namespace Someson\Grid\Viewer;

use Phalcon\DI;

/**
 * Class Url
 * Utility class, helps to build uri for \Someson\Grid\Button.
 * Used in \Someson\Grid\Viewer\ListLimiter, \Someson\Grid\Viewer\Paginator.
 *
 * @package Grid
 */
class Url
{
	protected static function getParams()
	{
		$router = DI::getDefault()->getShared('router');
		$result = [];
		if ($params = $router->getParams()) {
			foreach ($params as $param) $result[] = $param;
		}
		return implode('/', $result);
	}

	public static function getBasePrefix()
	{
		$router = DI::getDefault()->getShared('router');
		//$current = $router->getRewriteUri();
		//$name = $router->wasMatched() ? $router->getMatchedRoute()->getName() : '';

		$prefix = '/';
		if ($router->wasMatched()) {
			$group = $router->getMatchedRoute()->getGroup();
			if ($group) $prefix = $group->getPrefix();
		}

		$result = [];
		$p = trim($prefix, ' /');
		if (!empty($p)) $result[] = $p;
		if ($router->getControllerName() !== $router->getDefaultController()) {
			$result[] = $router->getControllerName();
		}
		if ($router->getActionName() !== $router->getDefaultAction()) {
			$result[] = $router->getActionName();
		}
		return implode('/', $result);
	}
}
