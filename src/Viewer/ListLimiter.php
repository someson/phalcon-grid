<?php
/**
 * Grid Phalcon PHP extension
 * PHP version 5.4+
 *
 * @license MIT http://opensource.org/licenses/MIT
 * @author  someson <somevov4ik@gmail.com>
 */

namespace Someson\Grid\Viewer;

use Someson\Grid\Button;
use Someson\Grid\ButtonGroup;
use Someson\Grid\Element;

/**
 * Class ListLimiter
 * Feature of Grid that realizes changeable list size, based on predefined
 * config options. Used to build html.
 *
 * @package Grid
 */
class ListLimiter extends Element
{
	public function __construct($limiterOptions = [])
	{
		$this->setOptions([
			'enabled'     => true,
			'limits'      => [10,25,50],
			'current'     => 10,
			'paramPrefix' => '',
		]);
		if ($limiterOptions) $this->setOptions($limiterOptions);
	}

	public function setEnabled($flag)
	{
		$this->setOption('enabled', !!$flag);
	}

	/**
	 * Implements interface method.
	 * Gets the rendered html data for output.
	 * @return string
	 */
	public function getValue()
	{
		return $this->render();
	}

	private function setUrl($limit)
	{
		$parts = [];
		//$parts[] = Url::getBasePrefix();
		if ($this->getParamPrefix()) $parts[] = $this->getParamPrefix();
		$parts[] = $limit;
		return implode('/', $parts);
	}

	public function render()
	{
		$buttons = [];
		foreach ($this->getOptions('limits') as $value) {
			$btn = new Button($value, $this->setUrl($value));
			$btn->htmlOptions(['class' => 'btn btn-default']);
			if ($value == $this->getCurrent()) $btn->appendHtmlOption('class', 'active');
			array_push($buttons, $btn);
		}
		return (new ButtonGroup($buttons))->getValue();
	}

	public function getMinLimit()
	{
		return $this->getOptions('limits')[0];
	}

	/**
	 * Implements interface method.
	 * Gets type of this object, namely the class name.
	 * @return string
	 */
	public function getType()
	{
		return __CLASS__;
	}
}
