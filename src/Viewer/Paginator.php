<?php
/**
 * Grid Phalcon PHP extension
 * PHP version 5.4+
 *
 * @license MIT http://opensource.org/licenses/MIT
 * @author  someson <somevov4ik@gmail.com>
 */

namespace Someson\Grid\Viewer;

use Someson\Grid\IDataProvider;
use Someson\Grid\Element;
use Someson\Grid\Button;
use Someson\Grid\ButtonGroup;

/**
 * Class Paginator
 * Feature of c.r.u.d. that realizes list paging. Used to build html.
 * Paging adapters used, based on DataProvider type.
 *
 * @package Grid
 */
class Paginator extends Element
{
	private $_paginator, $_listLimiter, $_initialized;

	protected $adapter = [
		IDataProvider::TYPE_MODEL => '\Phalcon\Paginator\Adapter\QueryBuilder',
		IDataProvider::TYPE_QUERY => '\Phalcon\Paginator\Adapter\QueryBuilder',
		IDataProvider::TYPE_ARRAY => '\Phalcon\Paginator\Adapter\NativeArray',
	];

	public function __construct($paginatorOptions = [], $limiterOptions = [])
	{
		$this->_initialized = false;
		$this->_listLimiter = new ListLimiter($limiterOptions);
		$currentLimit = $this->_listLimiter->getCurrent();
		$this->setOptions([
			'visibleButtons' => 5,
			'currentPage'    => 1,
			'itemsOnPage'    => $currentLimit,
			'paramPrefix'    => '',
		]);
		if ($paginatorOptions) $this->setOptions($paginatorOptions);
	}

	public function setOptions(array $options)
	{
		parent::setOptions($options);
		$this->getListLimiter()->setOptions([
			'current'     => $this->getOptions('itemsOnPage'),
			'paramPrefix' => $this->getOptions('paramPrefix'),
		]);
	}

	public function init(IDataProvider $dataProvider)
	{
		$type = $dataProvider->getType();
		$dataKey = in_array($type, [IDataProvider::TYPE_MODEL, IDataProvider::TYPE_QUERY]) ? 'builder' : 'data';
		$adapter = $this->adapter[$type];
		$options = [
			$dataKey => $dataProvider->getData(),
			'page'   => $this->getOptions('currentPage'),
			'limit'  => $this->getOptions('itemsOnPage'),
		];
		/** @var \Phalcon\Paginator\AdapterInterface $adapter */
		$adapter = new $adapter($options);
		$this->_paginator = $adapter->getPaginate();
		$this->_initialized = is_object($this->_paginator);
	}

	public function registerAdapter($name, $class)
	{
		$this->adapter[$name] = $class;
		return $this;
	}

	public function isInit()
	{
		return $this->_initialized;
	}

	public function getItems()
	{
		return $this->isInit() ? $this->_paginator->items : [];
	}

	public function getTotalPages()
	{
		return $this->isInit() ? $this->_paginator->total_pages : 0;
	}

	public function getNext()
	{
		return $this->isInit() ? $this->_paginator->next : 0;
	}

	public function getFirst()
	{
		return $this->isInit() ? $this->_paginator->first : 0;
	}

	public function getCurrent()
	{
		return $this->isInit() ? $this->_paginator->current : 0;
	}

	public function getLast()
	{
		return $this->isInit() ? $this->_paginator->last : 0;
	}

	public function getTotalItems()
	{
		return $this->isInit() ? $this->_paginator->total_items : 0;
	}

	public function getListLimiter()
	{
		return $this->_listLimiter;
	}

	/**
	 * Implements interface method.
	 * Gets the rendered html data for output.
	 * @return string
	 */
	public function getValue()
	{
		if (!$this->isInit()) return '';
		return $this->_render();
	}

	/**
	 * Implements interface method.
	 * Gets type of this object, namely the class name.
	 * @return string
	 */
	public function getType()
	{
		return __CLASS__;
	}

	private function setUrl($pageNum)
	{
		$parts = [];
//		$parts[] = Url::getBasePrefix();
		if ($this->getOptions('paramPrefix')) {
			$parts[] = $this->getOptions('paramPrefix');
		}
		$parts[] = $this->getListLimiter()->getCurrent();
		$parts[] = $pageNum;
		return implode('/', $parts);
	}

	private function _render()
	{
		$buttons = [];
		$visibleButtons = $this->getTotalPages() > $this->getOptions('visibleButtons') ?
			$this->getOptions('visibleButtons') : $this->getTotalPages();

		$showControls = $this->getTotalPages() > $visibleButtons;

		if ($showControls && $this->getCurrent() > 1) {
			$icon = '<i class="fa fa-angle-double-left"></i>';
			$btn = new Button($icon, $this->setUrl($this->getFirst()));
			$btn->htmlOptions(['class' => 'btn btn-default']);
			array_push($buttons, $btn);

			$icon = '<i class="fa fa-angle-left"></i>';
			$btn = new Button($icon, $this->setUrl($this->getCurrent()-1));
			$btn->htmlOptions(['class' => 'btn btn-default']);
			array_push($buttons, $btn);
		}

		$firstVisiblePage = 1;
		if ($this->getTotalPages() - $this->getOptions('visibleButtons')) {
			$paddingOffset = intval($this->getOptions('visibleButtons') / 2);

			if ($this->getCurrent() > $paddingOffset)
				$firstVisiblePage = $this->getCurrent() - $paddingOffset;

			if ($this->getCurrent() > $this->getLast() - $paddingOffset)
				$firstVisiblePage = $this->getLast() - $visibleButtons + 1;
		}
		for ($i = $firstVisiblePage, $j = 1; $j <= $visibleButtons; $i++, $j++) {
			$btn = new Button($i, $this->setUrl($i));
			$btn->htmlOptions(['class' => 'btn btn-default']);
			if ($i == $this->getCurrent()) $btn->appendHtmlOption('class', 'active');
			array_push($buttons, $btn);
		}

		if ($showControls && $this->getCurrent() < $this->getLast()) {
			$icon = '<i class="fa fa-angle-right"></i>';
			$btn = new Button($icon, $this->setUrl($this->getNext()));
			$btn->htmlOptions(['class' => 'btn btn-default']);
			array_push($buttons, $btn);

			$icon = '<i class="fa fa-angle-double-right"></i>';
			$btn = new Button($icon, $this->setUrl($this->getLast()));
			$btn->htmlOptions(['class' => 'btn btn-default']);
			array_push($buttons, $btn);
		}

		return $this->getTotalPages() > 1 ? (new ButtonGroup($buttons))->getValue() : '';
	}
}
