<?php
/**
 * Grid Phalcon PHP extension
 * PHP version 5.4+
 *
 * @license MIT http://opensource.org/licenses/MIT
 * @author  someson <somevov4ik@gmail.com>
 */

namespace Someson\Grid\Viewer;

use Someson\Grid\Element;

/**
 * Class Filter
 * Feature of Grid that realizes listing of filterable content, based on predefined
 * config options. Used to build html.
 *
 * @package Grid
 */
class Filter extends Element
{
	public function __construct()
	{
		$this->setOptions([
			'enabled' => true,
		]);
	}

	public function setEnabled($flag)
	{
		$this->setOption('enabled', !!$flag);
	}

	/**
	 * Implements interface method.
	 * Gets the rendered html data for output.
	 *
	 * @return string
	 */
	public function getValue()
	{
		return $this->render();
	}

	public function render()
	{
		/*
		$buttons = [];
		foreach ($this->getOptions('limits') as $value) {
			$btn = new Button($value, $this->setUrl($value));
			$btn->htmlOptions(['class' => 'btn btn-default']);
			if ($value == $this->getCurrent()) $btn->appendHtmlOption('class', 'active');
			array_push($buttons, $btn);
		}
		return (new ButtonGroup($buttons))->getValue();
		*/
		return '';
	}

	/**
	 * Implements interface method.
	 * Gets type of this object, namely the class name.
	 *
	 * @return string
	 */
	public function getType()
	{
		return __CLASS__;
	}
}
