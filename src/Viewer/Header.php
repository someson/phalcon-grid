<?php
/**
 * Grid Phalcon PHP extension
 * PHP version 5.4+
 *
 * @license MIT http://opensource.org/licenses/MIT
 * @author  someson <somevov4ik@gmail.com>
 */

namespace Someson\Grid\Viewer;

use Someson\Grid\Element;

/**
 * Class Header
 * Header structure with optionable sorting features.
 * Used to build html.
 *
 * @package Grid
 */
class Header extends Element
{
	private $_label, $_field, $_enabled;

	/**
	* Instantiates the new header object.
	* Predefines the default options.
	*
	* @param string $label Label to show in the output
	* @param string $field Field or composed fields, see example
	*
	* @example new \Someson\Grid\Viewer\Header('Adresse', 'postalCode.town');
	*/
	public function __construct($label, $field)
	{
		$this->_enabled = false;
		if (!is_numeric($label)) $this->setLabel($label);
		$this->_field = $field;
		$this->setOptions([
			'hidden' => false,
			'sortable' => !!$this->getValue(),
			'order' => 'asc',
		]);
	}

	public function getField()
	{
		return $this->_field;
	}

	public function setLabel($label)
	{
		$this->_label = trim($label);
	}

	public function getLabel()
	{
		return $this->_label;
	}

	public function isEnabled()
	{
		return $this->_enabled;
	}

	public function setEnabled($flag)
	{
		$this->_enabled = (bool) $flag;
	}

	// revert order if enabled
	public function getOrder()
	{
		$order = strtolower($this->getOptions('order'));
		return $this->isEnabled() ? ($order == 'asc' ? 'desc' : 'asc') : $order;
	}

	/**
	* Implements interface method.
	* Gets the stripped label from structure.
	*
	* @return string
	*/
	public function getValue()
	{
		return $this->_label ? strip_tags($this->_label) : '';
	}

	/**
	* Implements interface method.
	* Gets type of this object, namely the class name.
	*
	* @return string
	*/
	public function getType()
	{
		return __CLASS__;
	}

	public static function isValidPost($post)
	{
		return isset($post['field']) && isset($post['order']);
	}
}
