<?php
/**
 * Grid Phalcon PHP extension
 * PHP version 5.4+
 *
 * @license MIT http://opensource.org/licenses/MIT
 * @author  someson <somevov4ik@gmail.com>
 */

namespace Someson\Grid;

use Phalcon\Tag;

/**
 * Abstract class Element
 * Extendable parent class of output elements.
 *
 * @package Grid
 */
abstract class Element implements IElement
{
	use TraitConfigurable;

	protected $_htmlOptions = [];

	public function htmlOptions(array $options = [])
	{
		foreach ($options as &$value) $value = (array) $value;
		$this->_htmlOptions = self::mergeArray($this->_htmlOptions, $options);
		return $this;
	}

	public function appendHtmlOption($key, $value)
	{
		$o = $this->_htmlOptions;
		if (!isset($o[$key]) || !$o[$key]) $this->htmlOptions([$key => $value]);
		else $this->_htmlOptions[$key][] = $value;
		return $this;
	}

	public function removeHtmlOptions($options)
	{
		$options = (array) $options;
		foreach ($options as $option) {
			if (isset($this->_htmlOptions[$option])) unset($this->_htmlOptions[$option]);
		}
	}

	public function getAttr()
	{
		$attr = $this->_htmlOptions;
		foreach ($attr as &$v) $v = implode(' ', (array) $v);
		return $attr;
	}

	public function render()
	{
		return $this->getValue();
	}

	public function wrapWith($tag, $htmlOptions = [])
	{
		$out = Tag::tagHtml($tag, $htmlOptions);
		$out.= $this->render();
		return $out.Tag::tagHtmlClose($tag, true);
	}

	abstract public function getValue();
	abstract public function getType();
}
