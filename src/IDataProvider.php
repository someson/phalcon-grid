<?php
/**
 * Grid Phalcon PHP extension
 * PHP version 5.4+
 *
 * @license MIT http://opensource.org/licenses/MIT
 * @author  someson <somevov4ik@gmail.com>
 */

namespace Someson\Grid;

/**
 * Interface IDataProvider
 *
 * @package Grid
 */
interface IDataProvider
{
	const TYPE_MODEL = 'model';
	const TYPE_QUERY = 'query';
	const TYPE_ARRAY = 'array';

	/**
	 * @param array $options
	 * @return mixed
	 */
	public function getData($options = []);

	/**
	 * @return string
	 */
	public function getType();

	/**
	 * @param string $field
	 * @param string $type  ASC|DESC
	 * @return string
	 */
	public function orderCriteria($field, $type);
}
