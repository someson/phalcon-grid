<?php
/**
 * Grid Phalcon PHP extension
 * PHP version 5.4+
 *
 * @license MIT http://opensource.org/licenses/MIT
 * @author  someson <somevov4ik@gmail.com>
 */

namespace Someson\Grid;

/**
 * Trait TraitPatternBinder
 * Used to operate the data with mapping option.
 *
 * @package Grid
 */
trait TraitPatternBinder
{
	private static $_openElement  = '{';
	private static $_closeElement = '}';

	/**
	 * @param string $search
	 * @return array
	 */
	public static function getMatches($search)
	{
		$pattern = '/\\'.self::$_openElement.'[\s\w]+\\'.self::$_closeElement.'/uim';
		preg_match_all($pattern, $search, $matches, PREG_PATTERN_ORDER);
		array_walk_recursive($matches[0],
			create_function('&$v,$k','$v = trim($v,"'.self::$_openElement.self::$_closeElement.'");')
		);
		return $matches[0];
	}

	/**
	 * @param array $mapped
	 * @param array $matches
	 * @param string $where
	 * @return string
	 */
	public static function getBoundString(array $mapped, array $matches, $where)
	{
		$out = $where;
		foreach ($mapped as $search => $replace) {
			if (in_array($search, $matches)) {
				$what = self::$_openElement.$search.self::$_closeElement;
				$out = str_ireplace($what, $replace, $out);
			}
		}
		return $out;
	}
}
