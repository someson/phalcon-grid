<?php
/**
 * Grid Phalcon PHP extension
 * PHP version 5.4+
 *
 * @license MIT http://opensource.org/licenses/MIT
 * @author  someson <somevov4ik@gmail.com>
 */

namespace Someson\Grid;

use Someson\Grid\Viewer\Fields\Scalar;
use Someson\Grid\Viewer\Fields\Simple;
use Someson\Grid\Viewer\Fields\Custom;

/**
 * Trait TraitStructure
 * Used to operate the structure data
 *
 * @package Grid
 */
trait TraitStructure
{
	/**
	* Creates array structure for further internal using.
	*
	* @param array $structure Used by Viewer::create structure
	*
	* @see Viewer::create()
	* @return array
	*
	* simple => [
	*	0 => id
	*	1 => businessName
	*	2 => postalCode
	*	3 => town
	*	4 => tel
	* ]
	* complex => [
	*	ID      => [0 => id]
	*	Firma   => [0 => businessName]
	*	Adresse => [0 => postalCode, 1 => town]
	*	Tel.    => [0 => tel]
	* ]
	*/
	private static function _getUsedColumns(array $structure)
	{
		if (!is_array($structure) || !count($structure)) return false;
		$result = ['simple' => [], 'complex' => []];
		foreach ($structure as $key => $field) {
			if (is_array($field) && isset($field['value'])) $field = $field['value'];
			if ($field instanceof Simple) {
				foreach ($field->getUsedFields() as $v) {
					$result['simple'][] = $v;
					if (!isset($result['complex'][$key])) $result['complex'][$key] = [];
					$result['complex'][$key][] = $v;
				}
			}
		}
		return $result;
	}

	/**
	* Creates array structure for further internal using.
	*
	* @param array  $fields     Using self::_getUsedColumns 'complex' key
	* @param string $delimiter  Supposed structure. See Viewer::create
	*
	* @see Viewer::create()
	* @return array
	*
	* [
	*	ID      => id
	*	Firma   => businessName
	*	Adresse => postalCode+town
	*	Tel.    => tel
	* ]
	*/
	private static function _getReducedColumns(array $fields, $delimiter)
	{
		return array_map(
			function($value) use ($delimiter) {
				return implode($delimiter, $value);
			},
			$fields
		);
	}

	/**
	* Customizes the endpoint data $cell based on row data $data.
	* Used for resolving structure and formation the output.
	*
	* @param mixed $cell       Scalar|Simple|Custom object
	* @param array $data       Resolved full row data of DataProvider
	* @param array $itemsInfo  Pagination info used in callable as a second parameter
	*
	* @see Viewer::resolve()
	* @return Scalar|Viewer\Fields\Table|mixed
	*/
	private static function _customize($cell, $data = [], $itemsInfo = [])
	{
		switch (true) {
			case $cell instanceof Custom:
				return $cell->customize($data, $itemsInfo);
			case $cell instanceof Simple:
				$own = self::_findOwnData($cell->getUsedFields(), $data);
				$cell->compile($own);
				break;
			case $cell instanceof Button:
			case $cell instanceof ButtonGroup:
			default: break;
		}
		if (is_array($cell)) {
			$cell = isset($cell['value']) ? $cell['value'] : null;
		}
		if (is_scalar($cell)) return new Scalar($cell);

		// deep cloning instead of shallow cloning, because of GridButtonGroup
		return unserialize(serialize($cell)); // clone $cell;
	}

	/**
	* Finds and returns the field names used in current field object
	*
	* @param array $fields  array of used model|array keys in current field object
	* @param array $data    Resolved full row data of DataProvider
	*
	* @see self::_customize()
	* @return array
	*/
	private static function _findOwnData(array $fields, $data)
	{
		$out = [];
		foreach ($data as $k => $v) {
			if (in_array($k, $fields)) $out[$k] = $v;
		}
		return $out;
	}
}
