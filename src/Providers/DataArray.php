<?php
/**
 * Grid Phalcon PHP extension
 * PHP version 5.4+
 *
 * @license MIT http://opensource.org/licenses/MIT
 * @author  someson <somevov4ik@gmail.com>
 */

namespace Someson\Grid\Providers;

use Phalcon\DI;
use Someson\Grid\IDataProvider;
use Someson\Grid\TraitConfigurable;

/**
 * Class DataArray
 * Provides array data via DataProvider or directly.
 *
 * @package Grid
 */
class DataArray implements IDataProvider
{
	use TraitConfigurable;

	protected $_data, $_error;

	public function __construct(array $data)
	{
		$this->_data = (array) $data;
		$this->_error = [];
		$this->setOptions([
			'conditions' => false,
			'columns'    => false,
			'limit'      => false,
			'order'      => false, //'id ASC',
		]);
	}

	public function getType()
	{
		return IDataProvider::TYPE_ARRAY;
	}

	public function getData($options = [])
	{
		$this->setOptions($options);
		if ($columns = $this->getOptions('columns')) $this->_columns($columns);
		if ($conditions = $this->getOptions('conditions'))
			$this->setException('No conditional filtering support for arrays yet...');

		return $this->_data;
	}

	public function orderCriteria($field, $type)
	{
		usort($this->_data, function() use ($field) { return $this->_sorter($field); });
		if (strtolower($type) == 'desc') $this->_data = array_reverse($this->_data);
		$order = $field.' '.$type;
		$this->setOption('order', $order);
		return $order;
	}

	public function setException($message)
	{
		$this->_error[] = '<p>'.$message.'</p>';
		DI::getDefault()->get('flash')->notice(implode("\n", $this->_error));
	}

	private function _sorter($key)
	{
		return function ($a, $b) use ($key) {
			return strnatcmp($a[$key], $b[$key]);
		};
	}

	private function _columns($columns)
	{
		while (list($key, $value) = each($this->_data)) {
			$this->_data[$key] = array_intersect_key(
				$value, array_flip($columns)
			);
		}
	}
}
