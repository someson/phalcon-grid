<?php
/**
 * Grid Phalcon PHP extension
 * PHP version 5.4+
 *
 * @license MIT http://opensource.org/licenses/MIT
 * @author  someson <somevov4ik@gmail.com>
 */

namespace Someson\Grid\Providers;

use Someson\Grid\Exception;
use Someson\Grid\IDataProvider;
use Someson\Grid\TraitConfigurable;
use Phalcon\Mvc\Model\Resultset;
use Phalcon\Mvc\Model\Query\Builder;

/**
 * Class DataQuery
 * Provides model data via DataProvider or directly.
 *
 * @package Grid
 */
class DataQuery implements IDataProvider
{
	use TraitConfigurable;

	private $_builder;

	public function __construct($data)
	{
		if (!$data instanceof Builder) {
			throw new Exception('Input data must be of type Phalcon\Mvc\Model\Query\BuilderInterface');
		}
		$this->_builder = $data;
		$this->setOptions([
			'hydration'   => Resultset::HYDRATE_ARRAYS,
			'order'       => false, //'id ASC',
			'conditions'  => false,
			'columns'     => false,
			'limit'       => false,
		]);
	}

	public function getType()
	{
		return IDataProvider::TYPE_QUERY;
	}

	public function orderCriteria($field, $type)
	{
		$type = in_array(strtoupper($type), ['ASC','DESC']) ? $type : 'ASC';
		return sprintf('%s %s', $field, $type);
	}

	public function getData($options = [])
	{
		$this->setOptions($options);

		if ($order = $this->getOptions('order'))
			$this->_builder = $this->_builder->orderBy($order);

		return $this->_builder;
	}
}
