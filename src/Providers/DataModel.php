<?php
/**
 * Grid Phalcon PHP extension
 * PHP version 5.4+
 *
 * @license MIT http://opensource.org/licenses/MIT
 * @author  someson <somevov4ik@gmail.com>
 */

namespace Someson\Grid\Providers;

use Phalcon\Mvc\Model\Resultset;
use Phalcon\Db\Column;
use Phalcon\DI;

use Someson\Grid\IDataProvider;
use Someson\Grid\TraitModel;
use Someson\Grid\TraitConfigurable;

/**
 * Class DataModel
 * Provides model data via DataProvider or directly.
 *
 * @package Grid
 */
class DataModel implements IDataProvider
{
	use TraitModel, TraitConfigurable;

	protected $_modelName;

	public function __construct($data)
	{
		$this->_modelName = self::getModelName($data);
		$this->setOptions([
			'hydration'   => Resultset::HYDRATE_ARRAYS,
			'conditions'  => false,
			'columns'     => false,
			'limit'       => false,
			'order'       => false, // 'id ASC',
			'bind'        => false,
		]);
	}

	public function getType()
	{
		return IDataProvider::TYPE_MODEL;
	}

	public function orderCriteria($field, $type)
	{
		$stringTypes = [
			Column::TYPE_VARCHAR,
			Column::TYPE_CHAR,
			Column::TYPE_TEXT,
		];
		$typeOf = self::getDataTypes($this->_modelName);
		$type = in_array(strtoupper($type), ['ASC','DESC']) ? $type : 'ASC';

		// case insensitive sort for string field types
		return (isset($typeOf[$field]) && in_array($typeOf[$field], $stringTypes)) ?
			sprintf('LOWER(%s) %s', $field, $type) : sprintf('%s %s', $field, $type);
	}

	public function getData($options = [])
	{
		/** @var \Phalcon\Mvc\Model\Manager $modelsManager */
		$modelsManager = DI::getDefault()->getShared('modelsManager');
		$this->setOptions($options);

		/** @var \Phalcon\Mvc\Model\Query\BuilderInterface $builder */
		$builder = $modelsManager->createBuilder()->from($this->_modelName);

		if ($columns = $this->getOptions('columns')) {
			$builder = $builder->columns($columns);
		}
		if ($conditions = $this->getOptions('conditions')) {
//			$bind = $this->getOptions('bind');
//			$builder = $bind ? $builder->where($conditions, $bind) : $builder->where($conditions);
			$builder = $builder->where($conditions, $this->getOptions('bind') ?: []);
		}
		if ($order = $this->getOptions('order')) {
			$builder = $builder->orderBy($order);
		}

		return $builder;
	}
/*
	public function getData($options = [])
	{
		$name = $this->_modelName;
		$this->setOptions($options);

		$criteria = [];
		if ($columns = $this->getOptions('columns')) $criteria['columns'] = $columns;
		if ($conditions = $this->getOptions('conditions')) $criteria[] = $conditions;
		if ($order = $this->getOptions('order')) $criteria['order'] = $order;

		return $name::find($criteria);
	}
*/
}
