<?php

namespace Someson\Grid\Components;

use Phalcon\Paginator\Adapter;
use Phalcon\Paginator\Exception;

/**
 * Class SequencePaginator
 * Used to switch between of elements of ID-sequence, no data returned
 * @example
 * <code>
 * $paginator = new SequencePaginator([
 *   'sequence' => [1,5,6,8,11,35,3,9],
 *   'page'     => $currentPage,
 * ]);
 * $this->view->setVar('pager', $paginator->getPaginate());
 * </code>
 */
class SequencePaginator extends Adapter
{
	protected $_sequence; // [1,3,4,8,9,10,2,5,11, ...]

	public function __construct($config)
	{
		if (!is_array($config)) throw new Exception('Invalid parameter type');
		if (!isset($config['sequence'])) throw new Exception('Sequence is not defined');
		if (!is_array($config['sequence']) || !count($config['sequence']))
			throw new Exception('Sequence is ambiguous');

		$this->_config = $config;
		$this->_sequence = $config['sequence'];
		$this->_page = isset($config['page']) ? abs(intval($config['page'])) : 1;
	}

	public function setCurrentPage($page)
	{
		if (!is_int($page)) throw new Exception('Invalid parameter type.');
		$this->_page = $page;
	}

	public function getPaginate()
	{
		return (object) [
			'item'        => isset($this->_config['data']) ? $this->_config['data'] : null,
			'first'       => $this->_sequence[0],
			'last'        => $this->getLast(),
			'next'        => $this->getNext(),
			'before'      => $this->getPrevious(),
			'current'     => $this->_page,
			'total_pages' => $this->getSize(),
			'total_items' => $this->getSize(),
			'limit'       => $this->getLimit(),
		];
	}

	private function getSize()
	{
		return count($this->_sequence);
	}

	private function getLast()
	{
		return $this->_sequence[$this->getSize() - 1];
	}

	private function getNext()
	{
		if (in_array($this->_page, $this->_sequence)) {
			$key = array_search($this->_page, $this->_sequence);
			return isset($this->_sequence[$key + 1]) ? $this->_sequence[$key + 1] : $this->_sequence[$key];
		}
		return $this->getLast();
	}

	private function getPrevious()
	{
		if (in_array($this->_page, $this->_sequence)) {
			$key = array_search($this->_page, $this->_sequence);
			return isset($this->_sequence[$key - 1]) ? $this->_sequence[$key - 1] : $this->_sequence[0];
		}
		return $this->_sequence[0];
	}

	public function getLimit()
	{
		return 1;
	}
}
