<?php
/**
 * Grid Phalcon PHP extension
 * PHP version 5.4+
 *
 * @license MIT http://opensource.org/licenses/MIT
 * @author  someson <somevov4ik@gmail.com>
 */

namespace Someson\Grid;

use Phalcon\DI;
use Phalcon\Loader;
use Phalcon\Events\Event;
use Phalcon\Mvc\View;
use Phalcon\Mvc\User\Component;
use Phalcon\Config;
use Phalcon\Config\Adapter\Php as PhpConfig;

/**
 * Main Grid class
 *
 * @package Grid
 */
class Factory extends Component
{
	/** @var Viewer */
	public $viewer;
	/** @var Editor */
	public $editor;
	/** @var \Phalcon\Config  */
	private $_config;

	/**
	 * @param string $path
	 * @throws Exception
	 */
	public function __construct($path)
	{
		$path = rtrim($path, ' /').'/';
		$loader = new Loader();
		$loader->registerNamespaces([
			'Someson\Grid\Components'    => $path.'Components/',
			'Someson\Grid\Provider'      => $path.'Providers/',
			'Someson\Grid\Viewer'        => $path.'Viewer/',
			'Someson\Grid\Viewer\Fields' => $path.'Viewer/Fields/',
			'Someson\Grid\Editor'        => $path.'Editor/',
			'Someson\Grid\Editor\Fields' => $path.'Editor/Fields/',
		]);
		/** @var \Phalcon\Events\ManagerInterface $eventsManager */
		$eventsManager = DI::getDefault()->getShared('eventsManager');
/*
		$eventsManager->attach('loader', function($event, $loader) {
			if ($event->getType() == 'beforeCheckPath') {
				\Aus\Bootstrap::file($loader->getCheckedPath());
			}
		});
		$loader->setEventsManager($eventsManager);
*/
		$loader->register();

		$gridTemplates = __DIR__.'/Templates/';
		/** @var \Phalcon\Mvc\View $view */
		$view = $this->getDI()->getShared('view');
		if (!method_exists($view, 'addViewsDir')) {
			throw new Exception(sprintf('need to use views dir [%s] too', $gridTemplates));
		}
		$di = $this->getDi();
		$eventsManager->attach('view:notFoundView', function(Event $event, View $view) use ($di) {
			if ($view->isPicked()) {
				if ($view->getCurrentRenderLevel() == View::LEVEL_LAYOUT) {
					return;
				}
				/** @var \Phalcon\Flash $flash */
				$flash = $di->get('flash');
				$flash->setImplicitFlush(false);
				$content = 'Template <strong>' . str_replace('\\','/', $event->getData()) . '</strong> not found';
				$message = $flash->message('error', $content);
				$view->setContent($message . $view->getContent());
			}
		});
		$view->setEventsManager($eventsManager);
		$view->addViewsDir($gridTemplates);

		$config = $this->getDI()->getShared('config');
		$this->_config = $config->get('grid', []);
	}

	/**
	 * @return Viewer
	 * @throws \Phalcon\Config\Exception
	 */
	public function getViewer()
	{
		if ($this->viewer) return $this->viewer;
		$config = $this->_config ? $this->_config->get('viewer') : new PhpConfig('Config/Viewer.php');
		return $this->viewer = new Viewer($config);
	}

	/**
	 * @return Editor
	 * @throws \Phalcon\Config\Exception
	 */
	public function getEditor()
	{
		if ($this->editor) return $this->editor;
		$config = $this->_config ? $this->_config->get('editor') : new PhpConfig('Config/Editor.php');
		return $this->editor = new Editor($config);
	}
}
