<?php
/**
 * Grid Phalcon PHP extension
 * PHP version 5.4+
 *
 * @license MIT http://opensource.org/licenses/MIT
 * @author  someson <somevov4ik@gmail.com>
 */

namespace Someson\Grid\Editor;

use Someson\Grid\Components\SequencePaginator;
use Someson\Grid\TraitPatternBinder;

/**
 * Class Editor\Paginator
 * Makes possible to paginate a sequence of forms via url
 *
 * @package Grid
 */
class Paginator
{
	use TraitPatternBinder;

	/** @var SequencePaginator */
	protected $_paginator;
	/** @var string */
	protected $_url;

	public function __construct(array $options)
	{
		$this->_paginator = new SequencePaginator([
			'sequence' => $options['sequence'],
			'page' => array_key_exists('page', $options) ? $options['page'] : 1,
		]);
		$this->_url = $options['url'];
	}

	public function getPaginate()
	{
		$object = $this->_paginator->getPaginate();
		$object->nextUrl = self::getBoundString(['page' => $object->next], ['page'], $this->_url);
		$object->beforeUrl = self::getBoundString(['page' => $object->before], ['page'], $this->_url);
		return $object;
	}
}
