<?php
/**
 * Grid Phalcon PHP extension
 * PHP version 5.4+
 *
 * @license MIT http://opensource.org/licenses/MIT
 * @author  someson <somevov4ik@gmail.com>
 */

namespace Someson\Grid\Editor\Fields;

/**
 * Class WysiwygArea
 *
 * @package Grid
 */
class WysiwygArea extends TextArea
{
	public function __construct($name, $attributes = [])
	{
		parent::__construct($name, array_merge(['data-wysiwyg' => 'true'], $attributes));
	}
}
