<?php
/**
 * Grid Phalcon PHP extension
 * PHP version 5.6+
 *
 * @license MIT http://opensource.org/licenses/MIT
 * @author  someson <somevov4ik@gmail.com>
 */

namespace Someson\Grid\Editor\Fields;

use Phalcon\Validation\Validator\StringLength;
use Someson\Grid\Editor\TraitRelatable;
use Someson\Grid\Editor\TraitValidatable;

/**
 * Class Password
 *
 * @package Grid
 */
class Password extends \Phalcon\Forms\Element\Password
{
	use TraitRelatable, TraitValidatable;

	protected $_updatable;

	public function __construct($name, $attributes = null)
	{
		// chrome ignores these
		$extendedAttr = [
			'autocomplete'   => 'off',
			'autocorrect'    => 'off',
			'autocapitalize' => 'off',
		];
		parent::__construct($name, array_merge($attributes ?: [], $extendedAttr));
		$attributes = (array) $attributes;
		$readonly = in_array('readonly', $attributes) || in_array('disabled', $attributes);
		$this->_updatable = !$readonly;
		//$this->_noValue = true; // the value is not rendered

		$validators = [
			//new Emptyness(),
			new StringLength([
				'min' => 4,
				'max' => 10,
				'messageMaximum' => 'Maximum is: 10',
				'messageMinimum' => 'Minimum is: 4',
				'allowEmpty' => true,
			]),
		];
		$this->addValidators($validators);
	}

	/**
	 * @param array|null $attributes
	 * @param bool|false $useChecked
	 * @return array
	 */
	public function prepareAttributes(array $attributes = null, $useChecked = false)
	{
//		[
//			id    => 'grid_password',
//			class => 'form-control',
//			0     => 'password',
//			value => '$2a$12$64JdSfe5yYl86s1oi8bGNunF7OQfqHleoOQ/eKbtgnCRUu9kJHxo.',
//		]
		$default = parent::prepareAttributes($attributes);
		return array_merge($default, ['value' => null]);
	}
}
