<?php
/**
 * Grid Phalcon PHP extension
 * PHP version 5.4+
 *
 * @license MIT http://opensource.org/licenses/MIT
 * @author  someson <somevov4ik@gmail.com>
 */

namespace Someson\Grid\Editor\Fields;

use Phalcon\Tag;
use Someson\Grid\Editor\TraitRelatable;
use Someson\Grid\Editor\TraitValidatable;

/**
 * Class TextArea
 *
 * @package Grid
 */
class TextArea extends \Phalcon\Forms\Element\TextArea
{
	use TraitRelatable, TraitValidatable;

	protected $_updatable;

	public function __construct($name, $attributes = null)
	{
		$attributes = (array) $attributes;
		$readonly = in_array('readonly', $attributes) || in_array('disabled', $attributes);
		$this->_updatable = !$readonly;
		parent::__construct($name, $attributes);
	}

	// Fixes Phalcon 1.3.4 bug, where textarea has attribute "value"
	// No bug in Phalcon 2+
	public function render($attributes = null)
	{
		$attributes = $this->prepareAttributes($attributes);
		$content = $attributes['value'];
		unset($attributes['value']);

		$textarea = Tag::tagHtml('textarea', array_merge($attributes, ['name' => $this->getName()]));
		return $textarea . $content . Tag::tagHtmlClose('textarea');
	}
}
