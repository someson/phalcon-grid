<?php
/**
 * Grid Phalcon PHP extension
 * PHP version 5.4+
 *
 * @license MIT http://opensource.org/licenses/MIT
 * @author  someson <somevov4ik@gmail.com>
 */

namespace Someson\Grid\Editor\Fields;

use Someson\Grid\Editor\TraitRelatable;
use Someson\Grid\Editor\TraitValidatable;

/**
 * Class File
 * @package Grid
 */
class File extends \Phalcon\Forms\Element\File
{
	use TraitRelatable, TraitValidatable;

	protected $_updatable;

	public function __construct($name, $attributes = null)
	{
		parent::__construct($name, $attributes);
		$attributes = (array) $attributes;
		$readonly = in_array('readonly', $attributes) || in_array('disabled', $attributes);
		$this->_updatable = !$readonly;
		$this->_noValue = true; // the value is not rendered
	}
}
