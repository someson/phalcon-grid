<?php
/**
 * Grid Phalcon PHP extension
 * PHP version 5.4+
 *
 * @license MIT http://opensource.org/licenses/MIT
 * @author  someson <somevov4ik@gmail.com>
 */

namespace Someson\Grid\Editor\Fields;

use Phalcon\Tag;
use Phalcon\Forms\Exception;
use Phalcon\Forms\Element;
use Phalcon\Forms\ElementInterface;
use Someson\Grid\Editor\TraitRelatable;

/**
 * Class Text
 *
 * @package Grid
 */
class Group extends Element implements ElementInterface
{
	use TraitRelatable;

	protected $_tag, $_wrappable, $_attributes, $_counter;
	protected static $_groupCounter = 0;

	public function __construct($attributes = null)
	{
		if (is_array($attributes)) $this->_attributes = $attributes;
		$this->_wrappable = true;
		$this->_tag = isset($attributes['tag']) && $attributes['tag'] ? $attributes['tag'] : 'div';
		$this->_counter = ++self::$_groupCounter;
		$this->_attributes = array_merge($this->_attributes, ['id' => 'grid_'.$this->getName()]);
	}

	public function getName()
	{
		return 'groupField_'.$this->_counter;
	}
/*
	public function getRelatedFields()
	{
		$field = [];
		if (isset($this->_entityMap['field']) && is_array($this->_entityMap['field'])) {
			$map = $this->_entityMap['field'];
			foreach ($map as $fieldName => $className) {
				$field[$fieldName] = new $className($fieldName);
			}
		}
		return $field;
	}
*/
	public function render($attributes = null)
	{
		if (!is_null($attributes) && !is_array($attributes))
			throw new Exception('Invalid parameter type.');

		$attributes = array_merge($this->_attributes, $attributes);
		unset($attributes['tag']);

		$html = Tag::tagHtml($this->_tag, $attributes);
		return $html.Tag::tagHtmlClose($this->_tag);
	}
}
