<?php
/**
 * Grid Phalcon PHP extension
 * PHP version 5.4+
 *
 * @license MIT http://opensource.org/licenses/MIT
 * @author  someson <somevov4ik@gmail.com>
 */

namespace Someson\Grid\Editor\Fields;

use Someson\Grid\Editor\TraitRelatable;
use Someson\Grid\Editor\TraitValidatable;

/**
 * Class Select
 *
 * @package Grid
 */
class Select extends \Phalcon\Forms\Element\Select
{
	use TraitRelatable, TraitValidatable;

	protected $_updatable;

	public function __construct($name, $options = null, $attributes = null)
	{
		$attributes = (array) $attributes;
		$readonly = in_array('readonly', $attributes) || in_array('disabled', $attributes);
		$this->_updatable = !$readonly;
		parent::__construct($name, $options, $attributes);
	}
}
