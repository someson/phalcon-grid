<?php
/**
 * Grid Phalcon PHP extension
 * PHP version 5.4+
 *
 * @license MIT http://opensource.org/licenses/MIT
 * @author  someson <somevov4ik@gmail.com>
 */

namespace Someson\Grid\Editor\Fields;

use Phalcon\Validation\Validator\Email as EmailValidator;
use Phalcon\Validation\Validator\StringLength;
use Someson\Grid\Editor\TraitRelatable;
use Someson\Grid\Editor\TraitValidatable;

/**
 * Class Email
 * Extending of Phalcon\Forms\Element\Email
 *
 * @package Grid
 */
class Email extends \Phalcon\Forms\Element\Email
{
	use TraitRelatable, TraitValidatable;

	protected $_updatable;

	public function __construct($name, $attributes = null)
	{
		$attributes = (array) $attributes;
		$readonly = in_array('readonly', $attributes) || in_array('disabled', $attributes);
		$this->_updatable = !$readonly;
		parent::__construct($name, $attributes);

		$this->addValidators([
			new EmailValidator([
				'message' => 'The mail is not valid',
			]),
			new StringLength([
				'min' => 6,
				'max' => 100,
				'messageMaximum' => 'Maximum is: 100',
				'messageMinimum' => 'Minimum is: 6',
			]),
		]);
	}
}
