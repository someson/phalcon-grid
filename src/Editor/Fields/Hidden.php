<?php
/**
 * Grid Phalcon PHP extension
 * PHP version 5.4+
 *
 * @license MIT http://opensource.org/licenses/MIT
 * @author  someson <somevov4ik@gmail.com>
 */

namespace Someson\Grid\Editor\Fields;

use Someson\Grid\Editor\TraitRelatable;

/**
 * Class Text
 *
 * @package Grid
 */
class Hidden extends \Phalcon\Forms\Element\Hidden
{
	use TraitRelatable;

	protected $_updatable;

	public function __construct($name, $attributes = null)
	{
		parent::__construct($name, $attributes);
		$this->_updatable = true;
	}
}
