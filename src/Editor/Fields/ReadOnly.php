<?php
/**
 * Grid Phalcon PHP extension
 * PHP version 5.4+
 *
 * @license MIT http://opensource.org/licenses/MIT
 * @author  someson <somevov4ik@gmail.com>
 */

namespace Someson\Grid\Editor\Fields;

use Phalcon\Tag;
use Someson\Grid\TraitHelper;

/**
 * Class ReadOnly
 *
 * @package Grid
 */
class ReadOnly extends Text
{
	use TraitHelper;

	public function __construct($name, $attributes = null)
	{
		parent::__construct($name, $attributes);
		$this->_updatable = false;
	}

	public function render($attributes = null)
	{
		$changable = [
			'value' => $this->getAttribute('value'),
			'size' => 20,
		];
		$static = [
			'type' => 'text',
			'id' => 'grid_'.$this->getName(),
			'disabled' => 'disabled',
		];
		$attributes = self::mergeArray($changable, $attributes);
		$attributes = self::mergeArray($attributes, $static);
		return Tag::tagHtml('input', $attributes, true);
	}
}
