<?php
/**
 * Grid Phalcon PHP extension
 * PHP version 5.4+
 *
 * @license MIT http://opensource.org/licenses/MIT
 * @author  someson <somevov4ik@gmail.com>
 */

namespace Someson\Grid\Editor;

use Someson\Grid\Exception;
use Someson\Grid\Editor\Fields\Hidden;

/**
 * Trait TraitRelatable
 * Used as helper to extend merging data in the class.
 *
 * @package Grid
 */
trait TraitRelatable
{
	protected $_entityMap;

	public function getFieldName()
	{
		return $this->_entityMap ? $this->_entityMap['field'] : $this->getName();
	}

	public function hasRelatedModel()
	{
		return $this->_entityMap && isset($this->_entityMap['relation']);
	}

	public function getRelatedAliases($delimiter = '.')
	{
		return explode($delimiter, $this->_entityMap['relation']);
	}

	public function fromEntity(array $params)
	{
		if (!isset($params['field'])) throw new Exception('No [field] param defined');
		$this->_entityMap['field'] = $params['field'];

		if (isset($params['model'])) $this->_entityMap['model'] = $params['model'];
		if (isset($params['relation'])) $this->_entityMap['relation'] = $params['relation'];
		return $this;
	}

	public function updatable()
	{
		return isset($this->_updatable) && $this->_updatable;
	}

	public function wrappable()
	{
		return isset($this->_wrappable) && $this->_wrappable;
	}

	public function isTypeOfHidden()
	{
		return $this instanceof Hidden;
	}
}
