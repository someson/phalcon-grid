<?php
/**
 * Grid Phalcon PHP extension
 * PHP version 5.4+
 *
 * @license MIT http://opensource.org/licenses/MIT
 * @author  someson <somevov4ik@gmail.com>
 */

namespace Someson\Grid\Editor;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\ModelInterface;
use Phalcon\Forms\Element;
use Phalcon\Validation\Message;
use Phalcon\Validation\Message\Group as MessageGroup;

use Someson\Grid\Editor\Fields\Group as FieldGroup;
use Someson\Grid\Exception as GridException;

/**
 * Class Editor\Form
 * Creates a form with predefined input fields
 *
 * @package Grid
 */
class Form extends \Phalcon\Forms\Form
{
	protected $_mode, $_relations, $_structure, $_idPrefix, $_buttons;
	/** @var Model */
	protected $_model;
	/** @var Paginator */
	protected $_paginator;
	/** @var bool Show/hide "Submit" button */
	protected $_isEditable = true;
	protected $_isPageable = false;

	/**
	 * @param ModelInterface $model
	 * @param array $structure
	 * @param array|null $userOptions
	 */
	public function __construct(ModelInterface $model, array $structure, $userOptions = null)
	{
		$this->_idPrefix = 'grid_';
		$this->_model = $model;
		$this->_mode = $this->_model->getDirtyState();
		$this->_structure = $structure;
		$this->_options = $userOptions;
		$this->_buttons = [];
		parent::__construct($this->_model, $this->_options);
	}

	public function initialize()
	{
		$this->_relations = $this->getRelatedAliases();
		foreach ($this->_structure as $label => $field) {
			if ($field instanceof FieldGroup) {
				if ($field->hasRelatedModel()) {
					$alias = $field->getRelatedAliases();
					$relation = $this->getRelation($alias);
					$subFields = $field->getFieldName();
					foreach ($subFields as $fieldName => $className) {
						foreach ($relation as $relatedData) {
							/** @var \Phalcon\Forms\ElementInterface $relatedField */
							$relatedField = new $className($fieldName);
							$value = $relatedData->{$fieldName} ?: null;
							$relatedField->setAttribute('value', $value);
							$this->add($relatedField);
						}
					}
				}
			} else $this->addElement($label, $field);
		}
	}

	public function isEditMode()
	{
		return $this->_mode != Model::DIRTY_STATE_TRANSIENT;
	}

	public function setEditable($editable)
	{
		$this->_isEditable = (bool) $editable;
		if (!$this->_isEditable) {
			foreach ($this->getElements() as &$element) {
				if ($element instanceof Fields\Password) {
					$this->remove($element->getName());
					unset($this->_structure[$element->getLabel()]);
					continue;
				}
				$element->setAttribute('readonly', 'readonly');
			}
		}
		return $this;
	}

	public function isEditable()
	{
		return $this->_isEditable;
	}

	public function setPageable($pageable)
	{
		$this->_isPageable = (bool) $pageable;
	}

	public function isPageable()
	{
		return $this->_isPageable && is_object($this->_paginator);
	}

	public function createPaginator(array $options)
	{
		$this->_paginator = new Paginator($options);
		$this->_isPageable = true;
	}

	public function getPaginate()
	{
		return $this->_paginator->getPaginate();
	}

	public function getEditorElements()
	{
		return $this->_structure;
	}

	/**
	 * @param string $label
	 * @param Element|TraitRelatable $field
	 * @throws GridException
	 */
	public function addElement($label, Element $field)
	{
		if ($label) $field->setLabel($label);
		$field->setAttribute('id', $this->_idPrefix . $field->getName());
		if ($this->_model) {
			$value = $field->getAttribute('value');
			$fieldName = $field->getFieldName();
			if ($field->hasRelatedModel()) {
				$alias = $field->getRelatedAliases();
				if (!in_array($alias[0], $this->_relations)) {
					throw new GridException(sprintf(
						'Alias [%s] is not defined in model [%s]',
						$alias[0], get_class($this->_model)
					));
				}
				$relation = $this->getRelation($alias);
				$value = $relation ? $relation->{$fieldName} : null;
			} else $value = $this->isEditMode() ? $this->_model->{$fieldName} : $value;
			$field->setAttribute('value', $value);
		}
		$this->add($field);
	}

	/**
	 * @param array $relations
	 * @return null|object
	 * @throws GridException
	 */
	public function getRelation(array $relations)
	{
		$obj = $this->_model;
		foreach ($relations as $relation) {
			if (!is_string($relation)) throw new GridException('Relation name must be a string');
			$obj = $obj->{$relation};
		}
		return $obj;
	}

	/**
	 * @return array
	 * @throws GridException
	 */
	public function getRelatedAliases()
	{
		/** @var \Phalcon\Mvc\Model\ManagerInterface $manager */
		$manager = $this->_model->getModelsManager();
		$objRelations = $manager->getRelations(get_class($this->_model));
		$relations = [];
		foreach($objRelations as $obj) {
			$options = $obj->getOptions();
			if (!isset($options['alias'])) {
				throw new GridException(sprintf(
					'You should set an alias for the model [%s] to make updates',
					$obj->getReferencedModel()
				));
			}
			$relations[] = $options['alias'];
		}
		return $relations;
	}

	/**
	 * Redefines the message output.
	 * Must rethink this output logic because of https://github.com/phalcon/cphalcon/issues/11448
	 * @see https://github.com/phalcon/cphalcon/pull/11890
	 * @param string $name
	 */
	public function messages($name)
	{
		if ($this->hasMessagesFor($name) || $this->get($name)->hasMessages()) {
			$result = [];
			foreach ($this->getMessagesFor($name) as $message) {
				$result[] = sprintf('<p>%s</p>', $message->getMessage());
			}
			$this->flash->error(implode("\n", $result));
		}
	}

	/**
	 * @param string $field
	 * @param string|object $message
	 */
	public function appendMessage($field, $message)
	{
		$obj = $message;
		if (!isset($this->_messages[$field])) {
			$this->_messages[$field] = new MessageGroup();
			$obj = is_object($message) ?
				new Message($obj->getMessage(), $obj->getField(), $obj->getType(), $obj->getCode()) :
				new Message($message);
		}
		$this->_messages[$field]->appendMessage($obj);
	}

	public function addButton($htmlElement)
	{
		$this->_buttons[] = $htmlElement;
	}

	public function getButtons()
	{
		return $this->_buttons;
	}
}
