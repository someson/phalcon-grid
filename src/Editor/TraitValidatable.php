<?php
/**
 * Grid Phalcon PHP extension
 * PHP version 5.4+
 *
 * @license MIT http://opensource.org/licenses/MIT
 * @author  someson <somevov4ik@gmail.com>
 */

namespace Someson\Grid\Editor;

use Phalcon\Validation\Validator\PresenceOf;

/**
 * Trait TraitValidatable
 *
 * @package Grid
 */
trait TraitValidatable
{
	protected $_required;

	public function setRequired($flag, $message = null)
	{
		$this->_required = !!$flag;

		if ($this->_required) {
			/** @var \Phalcon\Forms\Element $this */
			$this->addValidator(new PresenceOf([
				'message' => $message ?: 'This field is required',
				'cancelOnFail' => true,
				'allowEmpty' => false,
			]));
		}

		return $this;
	}
}
