<?php
/**
 * Grid Phalcon PHP extension
 * PHP version 5.4+
 *
 * @license MIT http://opensource.org/licenses/MIT
 * @author  someson <somevov4ik@gmail.com>
 */

namespace Someson\Grid;

use Phalcon\Tag;

/**
 * Class Button
 * Creates button as element of c.r.u.d to switch between Viewer and Editor
 * or just forward|redirect to other url.
 *
 * @package Grid
 */
class Button extends Element implements ILinkable
{
	use TraitPatternBinder;

	private $_name, $_link, $_bind;

	public function __construct($name, $linkPattern)
	{
		$this->_name = $name;
		$this->_link = $linkPattern;
	}

	public function getLink()
	{
		return $this->_link;
	}

	public function setLink($link)
	{
		$this->_link = $link;
	}

	public function getBoundKeys()
	{
		return $this->_bind = $this->_bind ?: self::getMatches($this->getLink());
	}

	public function bind(array $values)
	{
		$link = self::getBoundString($values, $this->getBoundKeys(), $this->getLink());
		$this->setLink($link);
	}

	public function getName()
	{
		return $this->_name;
	}

	// interface method
	public function getType()
	{
		return __CLASS__;
	}

	// interface method
	public function getValue()
	{
		return $this->render();
	}

	public function render()
	{
		$params = [$this->getLink(), $this->getName()];
		if (count($this->getAttr())) {
			foreach ($this->getAttr() as $key => $param) $params[$key] = $param;
		}
		return Tag::linkTo($params);
	}

	public function isHidden()
	{
		return false;
	}
}
