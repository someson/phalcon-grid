{#% if showFlash is defined and showFlash %#}
{{ content() }}
{{ flashSession.output() }}
{#% endif %#}

<div class="grid-editor grid-border">
	{% if editor.isEditable() %}
		{{ form(null, 'method':'post', 'enctype':'multipart/form-data', 'role':'form', 'class':'form-horizontal') }}
	{% endif %}
	{% for label,field in editor.getEditorElements() -%}
		<div class="row">
			<div class="form-group{% if not field.updatable() %} has-feedback{% endif %} col-sm-12">
				<div class="row">
					<label for="{{ field.getAttribute('id') }}" class="col-sm-3 control-label" style="padding-right:0">{{ label }}</label>
					<div class="col-sm-9{% if editor.hasMessagesFor(field.getName()) %} has-error{% endif %}">
						{%- if field.wrappable() -%}
							{{ field.render(['class':'form-control']) }}
						{%- else -%}
							{{ editor.render(field.getName(), ['class':'form-control']) }}
							{%- if not field.updatable() -%}
								<span class="glyphicon glyphicon-lock form-control-feedback"></span>
							{%- endif -%}
							{{ editor.messages(field.getName()) }}
						{%- endif -%}
					</div>
				</div>
			</div>
		</div>
	{% endfor %}
	{%- for field in editor.getElements() -%}
		{%- if field.isTypeOfHidden() -%}
			{{ editor.render(field.getName()) }}
		{%- endif -%}
	{%- endfor %}
	{% if editor.isEditable() %}
		<div class="row">
			<div class="col-sm-3"></div>
			<div class="col-sm-9">
				<div class="crud-elements pull-left">
					<input type="submit" value="{{ __('Save') }}" class="btn btn-success search-btn" />
					{% if referer is defined -%}
					<a href="{{ referer }}" class="btn btn-warning">{{ __('Cancel') }}</a>
					{%- endif %}
					{% if delete is defined and editor.isEditMode() -%}
					<a class="btn btn-danger md-trigger" data-modal="delete-modal">{{ __('Delete') }}</a>
					{%- endif %}
					{% for otherButton in editor.getButtons() %}
					{{ otherButton }}
					{% endfor %}
				</div>
				{% if editor.isPageable() %}
				<div class="btn-group pull-right" role="group">
					{% set pager = editor.getPaginate() %}
					{% if pager.current !== pager.before %}
					<a href="{{ url(pager.beforeUrl) }}" class="btn btn-default">
						<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
					</a>
					{% endif %}
					{% if pager.current !== pager.next %}
					<a href="{{ url(pager.nextUrl) }}" class="btn btn-default">
						<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
					</a>
					{% endif %}
				</div>
				{% endif %}
			</div>
		</div>
	{% endif %}
	{% if editor.isEditable() %}
		{{ end_form() }}
	{% endif %}
</div>
