{{ content() }}
{{ flashSession.output() }}

{% if viewer is defined %}
	{% if viewer.count() %}

{% macro gridTools(that, viewer, position) %}
{% if viewer.toolsPosition(position) and viewer.toolsActive() %}
<div class="grid-tools">
	{% if viewer.isPaginator() %}
	<div class="btn-group grid-control grid-paginator">
		{{ viewer.getPaginator().render() }}
	</div>
	{%- endif %}

	{% if viewer.isSearcher() %}
	<div class="grid-control grid-search">
		{{ that.tag.form([null, 'method':'post', 'role':'form']) }}
			{{ viewer.getSearcher().render() }}
		{{ that.tag.endForm() }}
	</div>
	{% endif %}

	{% if viewer.isPaginator() %}
		{% if viewer.isListLimiter() and viewer.getPaginator().getListLimiter().isEnabled() %}
	<div class="btn-group grid-control grid-list-limiter">
		{{ viewer.getPaginator().getListLimiter().render() }}
	</div>
		{% endif %}
	{% endif %}
</div>
{% endif %}
{% endmacro %}

<div class="grid-border">
	{{ gridTools(this, viewer, 'top') }}
	<div class="table-responsive grid-viewer">
		<table class="table table-hover table-striped table-condensed grid-table">
		<thead>
			<tr>
			{% for head in viewer.getHead() if not head.isHidden() %}
				{% if head.isSortable() %}
				<th class="sortable">
					{% set order = head.getOrder() %}
					{{ form(null, 'method':'post', 'role':'form') }}
					<input type="hidden" name="field" value="{{ head.getField() }}" />
					<input type="hidden" name="order" value="{{ order }}" />
					<button type="submit" class="btn btn-success">
						<i class="fa fa-ellipsis-v"></i> {{ head.render() }}
						{% if head.isEnabled() -%}
						{% set icon = order == 'asc' ? 'up' : 'down' %}
						<i class="fa fa-chevron-{{ icon }}"></i>
						{%- endif -%}
					</button>
					{{ end_form() }}
				</th>
				{% else %}
				<th>{{ head.render() }}</th>
				{% endif %}
			{% endfor %}
			</tr>
		</thead>
		<tbody>
		{% set isBool = constant("Grid\Viewer\Field\Simple::TYPE_BOOL") %}
		{% for row in viewer.getBody() %}
			<tr>
			{% for cell in row if not cell.isHidden() %}
				<td{%- if instanceof(cell, 'Grid\Button') %} style="width:60px"{%- endif -%}>
				{%- if cell.getType() == isBool -%}
					{%- if cell.getValue() %}
					<span class="glyphicon glyphicon-ok"></span>
					{%- endif -%}
				{%- elseif instanceof(cell, 'Grid\ButtonGroup') -%}
					<div class="btn-group btn-count-{{ cell.getButtonsCount() }}" role="group">
						{{- cell.render() -}}
					</div>
				{%- else -%}
					{{- cell.render() -}}
				{%- endif -%}
				</td>
			{% endfor %}
			</tr>
		{% endfor %}
		</tbody>
		</table>
		<div class="grid-status">
			{% set status = viewer.getStatus() %}
			{{ __('Page') }} {{ status.current }} {{ __('from') }} {{ status.pages }}. {{ __('Total') }} {{ status.items }} {{ __('items found') }}.
		</div>
	</div>
	{{ gridTools(this, viewer, 'bottom') }}
</div>

	{% else %}
	<p class="widget" style="text-align:center;padding:20px 0;font-size:16px;">
		{{ __("There's no any data in the current list") }}
	</p>
	{% endif %}

	{%- if otherButtons is defined -%}
		{% for button in otherButtons -%}
		{{ button.render() }}
		{%- endfor %}
	{%- endif %}

	{%- if editor is defined -%}
		{{ partial('partials/grid_editor_default', ['editor':editor, 'showFlash':false]) }}
	{%- endif -%}

{% else %}
<p class="widget" style="text-align:center;padding:20px 0;font-size:16px;color:red;font-weight:700">
	{{ __("The list viewer is not defined") }}
</p>
{% endif %}

