<?php
/**
 * Grid Phalcon PHP extension
 * PHP version 5.4+
 *
 * @license MIT http://opensource.org/licenses/MIT
 * @author  someson <somevov4ik@gmail.com>
 */

namespace Someson\Grid;

use Phalcon\DI;
use Phalcon\Config;
use Phalcon\Mvc\Model;
use Phalcon\Mvc\ModelInterface;
use Phalcon\Mvc\Model\MetaData\Memory as MetaData;
use Phalcon\Forms\Element\Password;

/**
 * Class Editor
 * The main part of Grid (create + update + delete)
 * @package Grid
 *
 * @method bool isEditable()
 * @method bool isPageable()
 */
class Editor
{
	use TraitConfigurable;

	/** @var Editor\Form  */
	protected $_form;
	/** @var Config  */
	protected $_config;
	/** @var \Phalcon\Mvc\ModelInterface  */
	protected $_model;
	/** @var array */
	protected $_structure;

	public function __construct(Config $config)
	{
		$this->_config = $config;
		$this->setOptions([
			'editable' => true,
		]);
		$this->setOptions($this->_config->toArray());
	}

	/**
	 * @param ModelInterface $model
	 * @param array $structure
	 * @return Editor
	 * @throws \Someson\Grid\Exception
	 */
	public function init(ModelInterface $model, array $structure)
	{
		if (!$model) throw new Exception('The entry does not exist');
		$this->_model = $model;
		$this->_structure = $structure;
		$this->_form = new Editor\Form($this->_model, $this->_structure);
		$this->_form->setEditable($this->isEditable());
		return $this;
	}

	public function paginate(array $options)
	{
		if (!array_key_exists('sequence', $options) || !is_array($options['sequence']))
			throw new Exception('Sequence must be an array of numbers');

		if (array_key_exists('url', $options)) {
			$this->_form->createPaginator($options);
		}
		return $this;
	}

	/**
	 * @param array $formData
	 * @param array $extendedData Data assigned directly during execution
	 * @param bool|true $useNull
	 * @return bool
	 * @throws \Someson\Grid\Exception
	 */
	public function createData(array $formData, $extendedData = [], $useNull = true)
	{
		$referencedModel = [];
		foreach ($formData as $fieldName => $value) {
			if (!$this->getForm()->has($fieldName)) continue; // FF/IE Fix

			/** @var \Phalcon\Forms\ElementInterface|\Someson\Grid\Editor\TraitRelatable $field */
			$field = $this->getForm()->get($fieldName);
			$realName = $field->getFieldName();

			// set prepared field value
			$newValue = trim($value);

			if ($field instanceof Password) {
				$newValue = $newValue ?
					DI::getDefault()->getShared('security')->hash($newValue) :
					$this->getForm()->get($realName)->getAttribute('value');
//				if ($newValue == '') continue; // break value setting -> pass stays unchanged
//				$newValue = DI::getDefault()->getShared('security')->hash($newValue);
			}
			if ($useNull && $newValue == '') $newValue = null; // new RawValue('NULL');

			// set related field values
			if ($field->hasRelatedModel()) {
				$manager = $this->getModel()->getModelsManager();
				$objRelations = $manager->getRelations(get_class($this->getModel()));
				foreach ($objRelations as $obj) {
					$options = $obj->getOptions();
					$aliases = $field->getRelatedAliases();
					if (isset($options['alias']) && in_array($options['alias'], $aliases)) {
						$alias = $options['alias'];
						if (!isset($referencedModel[$alias])) {
							$referencedClass = $obj->getReferencedModel();
							$referencedModel[$alias] = new $referencedClass();
						}
						$referencedModel[$alias]->{$realName} = $newValue;
					}
				}
			} else $this->getModel()->$realName = $newValue;
		}
		if (count($referencedModel)) {
			foreach ($referencedModel as $relation => $instance) {
				$this->getModel()->$relation = $instance;
			}
		}

		// adding additional data without form validity check
		if ($extendedData) {
			$metadata = DI::getDefault()->getShared('modelsMetadata') ?: new MetaData();
			foreach ($extendedData as $fieldName => $value) {
				if ($metadata->hasAttribute($this->getModel(), $fieldName)) {
					$this->getModel()->$fieldName = $value;
				}
			}
		}
//		$this->refillForm($formData);
		return $this->saveEntry();
	}

	/**
	 * @param array $formData
	 * @param array $extendedData Data assigned directly during execution
	 * @param bool|true $useNull
	 * @return bool
	 * @throws \Someson\Grid\Exception
	 */
	public function updateData(array $formData, $extendedData = [], $useNull = true)
	{
		if (!$this->isEditable()) throw new Exception('Cannot handle the readonly form');

		// check form and prepare for db save
		foreach ($formData as $fieldName => $value) {
			if (!$this->getForm()->has($fieldName)) continue; // FF/IE Fix

			/** @var \Someson\Grid\Editor\TraitRelatable $field */
			$field = $this->getForm()->get($fieldName);
			$realName = $field->getFieldName();

			// set prepared field value
			$newValue = trim($value);

			if ($field instanceof Password) {
				$newValue = $newValue ?
					DI::getDefault()->getShared('security')->hash($newValue) :
					$this->getForm()->get($realName)->getAttribute('value');
//				if ($newValue == '') continue; // break value setting -> pass stays unchanged
//				$newValue = DI::getDefault()->getShared('security')->hash($newValue);
			}
			if ($useNull && $newValue === '') $newValue = null; //new RawValue('NULL');

			// set related field values
			if ($field->hasRelatedModel()) {
				$aliases = $field->getRelatedAliases();
				$relation = $this->getForm()->getRelation($aliases);
				if (!$relation) {
					throw new Exception(sprintf('Object for [%s] is not initialized yet', $fieldName));
				}
				$relation->{$realName} = $newValue;
			} else $this->getModel()->$realName = $newValue;
		}

		// adding additional data without form validity check
		if ($extendedData) {
			$metadata = DI::getDefault()->getShared('modelsMetadata') ?: new MetaData();
			foreach ($extendedData as $fieldName => $value) {
				if ($metadata->hasAttribute($this->getModel(), $fieldName)) {
					$this->getModel()->$fieldName = $value;
				}
			}
		}
		//$this->refillForm($formData);
		return $this->updateEntry();
	}

	/**
	 * @return bool
	 * @throws \Exception
	 */
	public function deleteData()
	{
		if (!$this->isEditable()) throw new \Exception('Cannot handle the readonly form');
		return $this->deleteEntry();
	}

	/**
	 * @return bool
	 * @throws \Exception
	 */
	private function saveEntry()
	{
		$model = $this->getModel();
		if (!$model->save()) {
			$messages = 'Cannot save data:<br />' . $this->flattenModelErrors();
			throw new \Exception($messages);
		}
		return Model::OP_CREATE == $model->getOperationMade() ?
			$model->getWriteConnection()->lastInsertId() : true;
	}

	/**
	 * @return bool
	 * @throws \Exception
	 */
	private function updateEntry()
	{
		return $this->saveEntry();
	}

	/**
	 * @return bool
	 * @throws \Exception
	 */
	private function deleteEntry()
	{
		if (!$this->getModel()->delete()) {
			$messages = 'Cannot delete data:<br />' . $this->flattenModelErrors();
			throw new \Exception($messages);
		}
		return true;
	}

	/**
	 * Transfer model error messages into form and make it outputable
	 * @return string
	 */
	private function flattenModelErrors()
	{
		/** @var \Phalcon\Mvc\Model\MessageInterface $error */
		foreach ($this->getModel()->getMessages() as $error) {
			$this->getForm()->appendMessage($error->getField(), $error);
		}
		$out = '';
		foreach ($this->getForm()->getMessages() as $i => $obj) {
			$out.= sprintf('%d) %s [%s]<br />', $i+1, $obj->getMessage(), $obj->getType());
		}
		return $out;
	}

	/**
	 * @param array $formData
	 */
	private function refillForm(array $formData)
	{
		foreach ($formData as $fieldName => $value) {
			if (!$this->getForm()->has($fieldName)) continue; // FF/IE Fix
			$field = $this->getForm()->get($fieldName);
			$field->setAttribute('value', $value);
		}
	}

	/**
	 * @return Editor\Form
	 */
	public function getForm()
	{
		return $this->_form;
	}

	/**
	 * @return \Phalcon\Mvc\Model
	 */
	public function getModel()
	{
		return $this->_model;
	}

	/**
	 * @param array $htmlElements
	 * @return $this
	 */
	public function addButtons(array $htmlElements = [])
	{
		foreach ($htmlElements as $button) {
			$this->getForm()->addButton($button);
		}
		return $this;
	}
}
