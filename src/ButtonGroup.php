<?php
/**
 * Grid Phalcon PHP extension
 * PHP version 5.4+
 *
 * @license MIT http://opensource.org/licenses/MIT
 * @author  someson <somevov4ik@gmail.com>
 */

namespace Someson\Grid;

/**
 * Class ButtonGroup
 * Creates group of buttons containig Someson\Grid\Button objects.
 *
 * @package Grid
 */
class ButtonGroup extends Element
{
	private $_group;

	public function __construct(array $buttons)
	{
		foreach ($buttons as $button) $this->_group[] = $button;
	}

	public function getCollection()
	{
		return $this->_group;
	}

	public function getValue()
	{
		return $this->render();
	}

	public function getType()
	{
		return __CLASS__;
	}

	public function getButtonsCount()
	{
		return count($this->_group);
	}

	public function render()
	{
		$out = '';
		foreach ($this->_group as $button) $out.= $button->getValue();
		return $out;
	}

	public function isHidden()
	{
		return false;
	}
}
