<?php
/**
 * Grid Phalcon PHP extension
 * PHP version 5.4+
 *
 * @license MIT http://opensource.org/licenses/MIT
 * @author  someson <somevov4ik@gmail.com>
 */

namespace Someson\Grid;

/**
 * Trait TraitOptionable
 * Used to define optionable environment to extend the used class
 *
 * @package Grid
 */
trait TraitConfigurable
{
	use TraitHelper;

	protected $_o = [];

	public function setOption($option, $value)
	{
		$this->_o[$option] = $value;
	}

	public function setOptions(array $options)
	{
		$this->_o = self::mergeArray($this->_o, $options);
	}

	public function getOptions($option = null)
	{
		$o = [];
		if ($option) {
			if (isset($this->_o[$option])) return $this->_o[$option];
		} else {
			foreach ($this->_o as $key => $value)
				if ($value !== false && !is_null($value)) $o[$key] = $value;
		}
		return $o;
	}

	public function __call($method, $args)
	{
		// check for existence in $_o for the key
		// isPaginator(), isListLimiter() etc.
		if ('is' === substr($method,0,2) && in_array(
			$m = lcfirst(substr($method,2)), array_keys($this->_o)
		)) return !!$this->_o[$m];

		// check the existence for the class variable
		// getHead(), getOptions(), getOptions('paginator') etc.
		if ('get' === substr($method,0,3)) {
			$used = lcfirst(substr($method,3));
			$name = '_'.$used;
			if (isset($this->$name)) {
				return count($args) && is_array($this->$name) ?
					$this->$name[$args[0]] : $this->$name;
			}
			if (isset($this->_o[$used])) return $this->_o[$used];
		}
		throw new \BadMethodCallException(sprintf('%s is not callable/allowed', $method));
	}
}
