<?php
/**
 * Grid Phalcon PHP extension
 * PHP version 5.4+
 *
 * @license MIT http://opensource.org/licenses/MIT
 * @author  someson <somevov4ik@gmail.com>
 */

namespace Someson\Grid;

/**
 * Trait TraitHelper
 * Used as helper to extend merging data in the class.
 *
 * @package Grid
 */
trait TraitHelper
{
	public static function mergeArray($a, $b)
	{
		$args = func_get_args();
		$out = array_shift($args);
		while (!empty($args)) {
			$next = array_shift($args);
			foreach ($next as $k => $v) {
				if (is_integer($k)) isset($out[$k]) ? $out[$k] = $v : $out[] = $v;
				else if(is_array($v) && isset($out[$k]) && is_array($out[$k]))
					$out[$k] = self::mergeArray($out[$k], $v);
				else $out[$k]=$v;
			}
		}
		return $out;
	}

	public static function isAssocArray($array)
	{
		return array_keys($array) !== range(0, count($array) - 1);
	}

	// [a => [b => [c => 111]]]          --> 111
	// [a => [b => [c => [111,222,333]]] --> [111,222,333]
	public static function reduceArray($array)
	{
		return (is_array($array) && count($array) == 1) ?
			self::reduceArray(reset($array)) : $array;
	}
}
