<?php

return [
	'tools' => [
		'position' => [
			'top' => false,
			'bottom' => true,
		],
		'elements' => [
			'paginator' => [
				'visibleButtons' => 5,
				'currentPage' => 1,
			],
			'listLimiter' => [
				'enabled' => true,
				'limits' => [5,10,25,50,100],
				'current' => 10,
			],
			'filter' => [
				'enabled' => true,
			],
		],
	],
];
