<?php
/**
 * Grid Phalcon PHP extension
 * PHP version 5.4+
 *
 * @license MIT http://opensource.org/licenses/MIT
 * @author  someson <somevov4ik@gmail.com>
 */

namespace Someson\Grid;

/**
 * Class DataProvider
 * Provides the data types for further using.
 *
 * @package Grid
 */
class DataProvider
{
	protected static $defaultAdapter = [
		IDataProvider::TYPE_MODEL => '\Someson\Grid\Providers\DataModel',
		IDataProvider::TYPE_QUERY => '\Someson\Grid\Providers\DataQuery',
		IDataProvider::TYPE_ARRAY => '\Someson\Grid\Providers\DataArray',
	];

	public function initNewAdapter($name, $adapterClass)
	{
		self::$defaultAdapter[$name] = $adapterClass;
	}

	public static function create($type, $data)
	{
		if (!isset(self::$defaultAdapter[$type])) throw new Exception(sprintf('Wrong DataProvider: %s', $type));
		return new self::$defaultAdapter[$type]($data);
	}
}
