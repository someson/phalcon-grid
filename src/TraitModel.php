<?php
/**
 * Grid Phalcon PHP extension
 * PHP version 5.4+
 *
 * @license MIT http://opensource.org/licenses/MIT
 * @author  someson <somevov4ik@gmail.com>
 */

namespace Someson\Grid;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\MetaData\Memory as MetaData;

/**
 * Trait TraitModel
 * Used by model field object to extend used class.
 *
 * @package Grid
 */
trait TraitModel
{
	public static function getModelName($model)
	{
		if ($model instanceof Model) return $model->getSource();
		$ns = trim($model, '\\ ');
		if (is_string($model) && class_exists('\\'.$ns)) return $ns;

		throw new Exception(sprintf('[%s] does not exist or not found', $model));
	}

	/**
	 * @param $model
	 * @return array
	 * @throws Exception
	 */
	public static function getDataTypes($model)
	{
		if (!$model) throw new Exception('Model is not defined');
		$model = is_string($model) ? new $model() : $model;
		$metaData = new MetaData();

		return $metaData->getDataTypes($model);
	}
}
