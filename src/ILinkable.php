<?php
/**
 * Grid Phalcon PHP extension
 * PHP version 5.4+
 *
 * @license MIT http://opensource.org/licenses/MIT
 * @author  someson <somevov4ik@gmail.com>
 */

namespace Someson\Grid;

/**
 * Interface ILinkable
 *
 * @package Grid
 */
interface ILinkable
{
	public function getLink();
	public function setLink($link);
}
