<?php
/**
 * Grid Phalcon PHP extension
 * PHP version 5.4+
 *
 * @license MIT http://opensource.org/licenses/MIT
 * @author  someson <somevov4ik@gmail.com>
 */

namespace Someson\Grid;

/**
 * Interface IElement
 *
 * @package Grid
 */
interface IElement
{
	public function getValue();
	public function getType();
}
